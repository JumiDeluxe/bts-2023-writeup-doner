# BtS-2023-Writeup-Doner

In this challenge we receive a file that appears to be an JPEG image.

<center>
<img src="images/image.jpg" width="300" height="300" alt="Image preview of challenge time">
</center>

There is a hint in the preview ("My cat loves cracking passwords"). \
Also result of `strings image.jpg` contain `tortilla.jpg`.

`Binwalk` detected encrypted `zip` archive. Let's extract it.
```
❯ binwalk -e image.jpg

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
17979         0x463B          Zip archive data, encrypted at least v2.0 to extract, compressed size: 70812, uncompressed size: 73631, name: tortilla.jpg
88959         0x15B7F         End of Zip archive, footer length: 22
```

We got a password protected archive, let's crack it.
```
❯ unzip 463B.zip
Archive:  463B.zip
[463B.zip] tortilla.jpg password: 
```

I will try to crack the password using `john` tools.

```
❯ zip2john 463B.zip > hash.txt
ver 2.0 efh 5455 efh 7875 463B.zip/tortilla.jpg PKZIP Encr: 2b chk, TS_chk, cmplen=70812, decmplen=73631, crc=C0333A52
❯ john --show hash.txt
463B.zip/tortilla.jpg:iloveyou:tortilla.jpg:463B.zip::463B.zip

1 password hash cracked, 0 left
```

Password was very easy, the archive contains just another image and it's `tortilla.jpg`.

<center>
<img src="images/tortilla.jpg" width="300" height="210" alt="Extracted image preview">
</center>

It appears to be just to be some unmodified free stock image, however it contains a lot of metadata that this kind of image shouldn't have.

```
❯ exiftool tortilla.jpg
perl: warning: Setting locale failed.
perl: warning: Please check that your locale settings:
	LANGUAGE = (unset),
	LC_ALL = (unset),
	LC_MEASUREMENT = "pl_PL.UTF-8",
	LC_MONETARY = "pl_PL.UTF-8",
	LC_TIME = "pl_PL.UTF-8",
	LANG = "en_US.UTF-8"
    are supported and installed on your system.
perl: warning: Falling back to a fallback locale ("en_US.UTF-8").
ExifTool Version Number         : 12.60
File Name                       : tortilla.jpg
Directory                       : .
File Size                       : 74 kB
File Modification Date/Time     : 2023:05:20 21:24:22+02:00
File Access Date/Time           : 2023:06:11 18:59:06+02:00
File Inode Change Date/Time     : 2023:06:11 18:59:03+02:00
File Permissions                : -rw-r--r--
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
Exif Byte Order                 : Big-endian (Motorola, MM)
X Resolution                    : 72
Y Resolution                    : 72
Resolution Unit                 : inches
Y Cb Cr Positioning             : Centered
GPS Version ID                  : 2.3.0.0
GPS Latitude Ref                : North
XMP Toolkit                     : Image::ExifTool 12.57
GPS Longtitude                  : 17 deg 3' 18.88" E
Image Width                     : 600
Image Height                    : 420
Encoding Process                : Baseline DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:0 (2 2)
Image Size                      : 600x420
Megapixels                      : 0.252
GPS Latitude                    : 51 deg 6' 37.14" N
```
GPS coordinates are always interesting so let's check them. \
Luckily Google Maps supports searching by coordinates.

We got a quite interesting location:
<center>
<img src="images/maps.png" width="600" height="370" alt="GPS coordinates lookup in Google Maps">
</center>

This is a kebab street food located near our CTF onsite localisation.

Let's check reviews while we're at it.

<center>
<img src="images/reviews.png" width="300" height="470" alt="Reviews from the Google Maps place">
</center>

We got our flag!